#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>

int main(){
	int fd;
	char buf[4];

	fd = open("/dev/mydev", O_RDWR);
	write(fd,"kkt\n",4);
	close(fd);

	fd = open("/dev/mydev", O_RDWR);
	read(fd,buf,4);
	puts(buf);
	close(fd);
}
