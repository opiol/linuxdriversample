
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/semaphore.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <asm/uaccess.h>	/* for put_user */

#define DEVICE_NAME "mydev"

static struct fake_device {
	char data[100];
	u32 count;
} virtual_device;

struct cdev *mcdev;
int major_number;
int ret;
dev_t dev_num;

int device_open(struct inode *inode, struct file *filp){
	printk(KERN_INFO "opened device");
	return 0;
}

ssize_t device_read(struct file* filp, char *bufStoreData, size_t bufCount, loff_t *curOffset){
	printk(KERN_INFO "reading..%s ,%d",virtual_device.data,virtual_device.count);
	ret = copy_to_user(bufStoreData, virtual_device.data,virtual_device.count);
	printk(KERN_INFO "ret: %d",ret);
	return ret;
} 

ssize_t device_write(struct file *filp, const char* bufSourceData, size_t bufCount, loff_t* curOffset){
	printk(KERN_INFO "writing...");
	ret = copy_from_user(virtual_device.data, bufSourceData, bufCount);
	virtual_device.count = bufCount;
	printk(KERN_INFO "ret:%d count:%d",ret,virtual_device.count);
	return ret;
}

int device_close(struct inode *inode, struct file *filep){
	printk(KERN_INFO "device closed");
	return 0;
}

struct file_operations fops = {
	.owner = THIS_MODULE,
	.open = device_open,
	.release = device_close,
	.write = device_write,
	.read = device_read
};
/************** /proc entry ******************/

static int hello_proc_show(struct seq_file *m, void *v) {
  seq_printf(m, "Hello proc!\n");
  return 0;
}

static int hello_proc_open(struct inode *inode, struct  file *file) {
  return single_open(file, hello_proc_show, NULL);
}

static const struct file_operations hello_proc_fops = {
  .owner = THIS_MODULE,
  .open = hello_proc_open,
  .read = seq_read,
  .llseek = seq_lseek,
  .release = single_release,
};

/*****************/
static int driver_entry(void){
	printk(KERN_ALERT "starting");
	ret = alloc_chrdev_region(&dev_num,0,1,DEVICE_NAME);
	if (ret < 0){
		printk(KERN_ALERT "failed to alocate num");
		return ret;
	}
	major_number = MAJOR(dev_num);
	printk(KERN_INFO "major number: %d",major_number);
	
	mcdev = cdev_alloc();
	mcdev->ops = &fops;
	mcdev->owner = THIS_MODULE;
	
	ret = cdev_add(mcdev, dev_num,1);
	if (ret < 0){
		printk(KERN_ALERT "unable to add cdev to kernel");
		return ret;
	}

	proc_create("mydev", 0, NULL, &hello_proc_fops);
	return 0;
}

static void driver_exit(void){
	cdev_del(mcdev);
	unregister_chrdev_region(dev_num,1);
	remove_proc_entry("hello_proc", NULL);
	printk(KERN_ALERT "unloaded module");
}





module_init(driver_entry);
module_exit(driver_exit);
